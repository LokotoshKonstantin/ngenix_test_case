# 2. Обрабатывает директорию с полученными zip архивами, 
# разбирает вложенные xml файлы и формирует 2 csv файла:

# Первый: id, level - по одной строке на каждый xml файл
# Второй: id, object_name - по отдельной строке для каждого тэга object 
# (получится от 1 до 10 строк на каждый xml файл)

# Очень желательно сделать так, чтобы задание 2 эффективно использовало 
# ресурсы многоядерного процессора.

import time
import threading
import os
import glob
import csv
from zipfile import ZipFile
import xml.etree.ElementTree as ET

start = time.perf_counter()


def get_files_from_dir(dirname: str = ".") -> list:
    """Получем список имен архивов в директории.

    Args:
        dirname (str, optional): Имя директории. Defaults to ".".

    Returns:
        list: list с именами файлов архивов в этой директрии.
    """
    list_of_arhivies_from_dir = []
    if os.path.isdir(dirname):
        list_files = glob.glob(dirname + "/*.zip")
        if list_files:
            for file in list_files:
                list_of_arhivies_from_dir.append(file)
        else:
            print("Error. Directory does not contain .zip files.")
    else:
        print("Error. Directory is not exist.")
        return False
    return list_of_arhivies_from_dir


def append_data_to_csv(data: list, csv_filename: str = "./1.csv",
                       flag_group_data=False):
    """_summary_

    Args:
        data (list): _description_
        csv_filename (str, optional): _description_. Defaults to "./1.csv".
        flag_group_data (bool, optional): _description_. Defaults to False.
    """
    if flag_group_data:
        with open(csv_filename, 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(data)
    else:
        with open(csv_filename, 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(data)


def read_archive_and_parse_xml(cur_archive : str):
    """Читаем файлы xml из архива. Парсим xml файл.
    Записываем полученные данные в csv.

    Args:
        cur_archive (str): Имя файла, который обрабатываем в текущий момент.
    """
    with ZipFile(file=cur_archive, mode="r") as archive:
        for cur_index in range(100):
            cur_xml = archive.open("cur_file_" + str(cur_index) + ".xml")
            tree = ET.parse(cur_xml)
            id = tree.findall("var")[0].get("value")  # Получаем id
            level = tree.findall("var")[1].get("value")  # Получаем level

            append_data_to_csv(data=[id, level], csv_filename="1.csv",
                               flag_group_data=False)

            objects = tree.findall("objects")
            list_objects = objects[0].findall("object")
           
            # Групируем данные objects
            objects_data = []
            for cur_object in list_objects:
                objects_data.append([id, cur_object.get("name")])

            append_data_to_csv(data=objects_data, csv_filename="2.csv",
                               flag_group_data=True)


def init_csv_files():
    """Иницилизация двух пустых csv файла, для записи результата.
    """
    with open('1.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["id", "level"])

    with open('2.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["id", "object_name"])


if __name__ == '__main__':
    init_csv_files()

    list_of_archives = get_files_from_dir(dirname=os.getcwd() + "/archives")
    if list_of_archives:
        threads = []
        for cur_archive in list_of_archives:
            t = threading.Thread(target=read_archive_and_parse_xml,
                                 args=[cur_archive])
            t.start()
            threads.append(t)

        for t in threads:
            t.join()

        print("The result has been successfully written to " \
              "files 1.csv and 2.csv.")
    else:
        print("Error.")
