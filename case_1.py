# Создает 50 zip-архивов, в каждом 100 xml файлов со случайными данными
#  следующей структуры:
# <root><var name=’id’ value=’<случайное уникальное строковое значение>’/>
# <var name=’level’ value=’<случайное число от 1 до 100>’/>
# <objects>
# <object name=’<случайное строковое значение>’/>
# <object name=’<случайное строковое значение>’/>…</objects></root>

# В тэге objects случайное число (от 1 до 10) вложенных тэгов object.


from zipfile import ZipFile
import xml.etree.ElementTree as ET
import string
import os
from random import randint, choice

letters = string.ascii_lowercase


def generate_random_str() -> str:
    """_summary_

    Returns:
        str: _description_
    """
    return ''.join((choice(letters)) for x in range(20))


def create_xml_structure(cur_index: int = 0) -> str:
    """Функция для создания XML структуры, с рандомнными значениями.

    Returns:
        XML: XML структура
    """
    random_int = randint(a=1, b=100)
    random_count_objects = randint(a=1, b=10)

    root = ET.Element('root')

    var = ET.SubElement(root, 'var')
    var.set('name', 'id')
    var.set('value', generate_random_str())

    var = ET.SubElement(root, 'var')
    var.set('name', 'level')
    var.set('value', str(random_int))

    objects = ET.SubElement(root, 'objects')
    for index in range(random_count_objects):
        item1 = ET.SubElement(objects, 'object')
        item1.set('name', generate_random_str())

    mydata = ET.tostring(root, encoding='unicode')
    filename = "cur_file_" + str(cur_index) + ".xml"
    with open(filename, "w") as file:
        file.write(mydata)

    return filename


def remove_xml_file(filename: str = "") -> bool:
    """Функция удаление xml файлов.
       Применяется после того, как мы уже создали файлы и записали их
       в архив. Чтобы не оставлять ненужных xml файлы в директории. 

    Args:
        filename (str, optional): Имя файла в файловой системе. Defaults to "".

    Returns:
        bool: True - успешно удалили файла
    """
    
    if os.path.exists(filename):
        os.remove(filename)
        return True
    return False


def check_dir_exist():
    """Проверяем, существует ли директория для хранения архивов.
    Если директории ./archives нет, то создаем.
    """
    dir_name = os.getcwd() + "/archives"
    if not os.path.isdir(dir_name):
        os.makedirs(dir_name)
        print("Сreated a directory ./arhcives")
    else:
        print("Directory ./arhcives exist.")


if __name__ == '__main__':
    check_dir_exist()

    for archive_index in range(50):
        with ZipFile("./archives/archive_" + str(archive_index) + ".zip", 
                     mode="w") as cur_zip:
            for file_index in range(100):
                filename_new_xml_file = create_xml_structure(file_index) 
                cur_zip.write(filename=filename_new_xml_file)
                if not remove_xml_file(filename=filename_new_xml_file):
                    print("Error delete file " + str(filename_new_xml_file))

    print("Successfully created archives. Pls go to ./arhcives/ directory.")